@ECHO OFF

:: Skrypt `required.cmd` dodaje i instaluje wymagane moduły przy pomocy NPM,
:: a następnie uruchamia polecenie `yarn install` w celu wygenerowania pliku "yarn.lock".

:: Przed uruchomieniem skryptu należy usunąć:
:: - folder "node_modules"
:: - plik "package-lock.json"
:: - plik "yarn.lock"
:: - w pliku "package.json" sekcję `dependencies` i `devDependencies`

npm install --save-prod priv-fields@1.1.x && npm install --save-dev express@4.16.x serve-index@1.9.x fs-extra@7.0.x browserify@16.2.x typescript@3.2.x rollup@1.1.x rollup-plugin-typescript2@0.19.x rollup-plugin-commonjs@9.2.x rollup-plugin-node-resolve@4.0.x rollup-plugin-json@3.1.x rollup-plugin-sourcemaps@0.4.x rollup-plugin-babel@4.3.x @babel/core@7.2.x @babel/cli@7.2.x @babel/preset-env@7.3.x uglify-js@3.4.x typedoc@0.14.x nodemon@1.18.x && yarn install