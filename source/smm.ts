// Simple Modules Manager

const globalObject: any = (typeof window === 'object') ? window : (typeof global === 'object' ? global : (typeof self === 'object' ? self : {}));

const alias = {
    hasReturned: "[[hasReturned]]",
    hasInit: "[[hasInit]]"
}

function addAsProperty(ref: Array<any>, element: any): void {
    if ((typeof element === 'function') && (element[alias.hasReturned] !== true)) {
        const result: any = element.call(null, ref);

        let name: string = element.name || "";
        if (name === "") {
            name = element.toString().match(/^function\s*([^\s(]+)/)[1] + "";
        }

        if (typeof result !== 'undefined') {
            element[alias.hasReturned] = true;

            if (name.length > 0) {
                (<any>ref)['@' + name] = result;
            }
        }
    }
}

function walkForElements(ref: Array<any>): void {
    let index: number = 0;
    while (index < ref.length) {
        const element: any = ref[index];
        addAsProperty(ref, element);
        index++;
    }
}

function installInitFunction(sm: any): void {
    sm.newSMM = function (ctx: any, name: string): void {
        init(ctx, name);
    };
}

function installPushMethod(sm: any): void {
    sm.push = function (...args: Array<any>) {
        const result: number = Array.prototype.push.call(sm, ...args);
        addAsProperty(this, args[0] || null);
        walkForElements(this);
        return result;
    }
}

export function init(ctx: any, name: string): void {
    let sm: any = [];

    if (ctx === 'global') {
        if (globalObject[name] instanceof Array) {
            sm = globalObject[name];
        } else {
            globalObject[name] = sm;
        }

        if (sm[alias.hasInit] !== true) {
            installInitFunction(sm);
        }
    } else {
        if (ctx[name] instanceof Array) {
            sm = ctx[name];
        } else {
            ctx[name] = sm;
        }
    }

    if (sm[alias.hasInit] !== true) {
        sm[alias.hasInit] = true;
        installPushMethod(sm);
    }
}