import { init } from "../source/smm";

///// INIT:
if (typeof init !== 'function') {
    throw 'quqeBm';
}

const objectX: any = {};
init(objectX, 'modules');

if (!(objectX.modules instanceof Array)) {
    throw 'hTvCGY';
}

init('global', 'modules');

const globalObject: any = (typeof window === 'object') ? window : (typeof global === 'object' ? global : (typeof self === 'object' ? self : {}));

if (!(globalObject.modules instanceof Array)) {
    throw 'pactES';
}


///// ADD MODULES:
declare const modules: any;

// A:
modules.push(function moduleA() {
    return {
        name: "A"
    }
});

// C:
modules.push(function moduleC(_m: any): any {
    if (_m['@moduleA'] && _m['@moduleB']) {
        return {
            name: "C"
        }
    }
});

// B:
modules.push(function moduleB(_m: any): any {
    if (_m['@moduleA']) {
        return {
            name: "B"
        }
    }
});

// TEST:
if (typeof modules['@moduleA'] !== 'object') {
    throw 'xrjtOm';
}

if (typeof modules['@moduleB'] !== 'object') {
    throw 'z8fqXv';
}

if (typeof modules['@moduleC'] !== 'object') {
    throw 'W98YrV';
}

if (modules['@moduleC'].name !== "C") {
    throw 'YJ82uz';
}