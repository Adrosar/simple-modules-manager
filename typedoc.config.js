const helpers = require('./lib/helpers');

module.exports = {
    out: helpers.projectResolve('typedoc'),
    mode: 'file',
    module: 'commonjs',
    target: 'es5',
    exclude: [
        helpers.projectResolve('.rpt2_cache/**/*'),
        helpers.projectResolve('.temp/**/*'),
        helpers.projectResolve('build/**/*'),
        helpers.projectResolve('dist/**/*'),
        helpers.projectResolve('lib/**/*'),
        helpers.projectResolve('node_modules/**/*'),
        helpers.projectResolve('test/**/*'),
        helpers.projectResolve('web/**/*'),
    ],
    includes: [
        helpers.projectResolve('source'),
    ],
    experimentalDecorators: true,
    excludeExternals: true,
    excludeNotExported: true,
    excludePrivate: true,
};