"use strict";
// Simple Modules Manager
Object.defineProperty(exports, "__esModule", { value: true });
const globalObject = (typeof window === 'object') ? window : (typeof global === 'object' ? global : (typeof self === 'object' ? self : {}));
const alias = {
    hasReturned: "[[hasReturned]]",
    hasInit: "[[hasInit]]"
};
function addAsProperty(ref, element) {
    if ((typeof element === 'function') && (element[alias.hasReturned] !== true)) {
        const result = element.call(null, ref);
        let name = element.name || "";
        if (name === "") {
            name = element.toString().match(/^function\s*([^\s(]+)/)[1] + "";
        }
        if (typeof result !== 'undefined') {
            element[alias.hasReturned] = true;
            if (name.length > 0) {
                ref['@' + name] = result;
            }
        }
    }
}
function walkForElements(ref) {
    let index = 0;
    while (index < ref.length) {
        const element = ref[index];
        addAsProperty(ref, element);
        index++;
    }
}
function installInitFunction(sm) {
    sm.newSMM = function (ctx, name) {
        init(ctx, name);
    };
}
function installPushMethod(sm) {
    sm.push = function (...args) {
        const result = Array.prototype.push.call(sm, ...args);
        addAsProperty(this, args[0] || null);
        walkForElements(this);
        return result;
    };
}
function init(ctx, name) {
    let sm = [];
    if (ctx === 'global') {
        if (globalObject[name] instanceof Array) {
            sm = globalObject[name];
        }
        else {
            globalObject[name] = sm;
        }
        if (sm[alias.hasInit] !== true) {
            installInitFunction(sm);
        }
    }
    else {
        if (ctx[name] instanceof Array) {
            sm = ctx[name];
        }
        else {
            ctx[name] = sm;
        }
    }
    if (sm[alias.hasInit] !== true) {
        sm[alias.hasInit] = true;
        installPushMethod(sm);
    }
}
exports.init = init;
