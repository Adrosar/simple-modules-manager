# Simple Modules Manager

## ❖ Installation ❖

From  [npmjs.com](https://www.npmjs.com) _(recommended)_ - choose one of the options:

 - `npm install simple-modules-manager`
 - `yarn add simple-modules-manager`

From [Adrosar/simple-modules-manager](https://bitbucket.org/Adrosar/simple-modules-manager) - downloads the entire contents of the repository:
```
npm install bitbucket:Adrosar/simple-modules-manager
```


## ❖ Use in browser ❖
**[1]** Run in terminal code `npm run rollup:dist` or `npm run browserify:dist` _(Generating  file JS for browser)_.

**[2]** In page (HTML file) insert code:

```html
<script type="text/javascript" src="dist/index.min.js" async></script>
```

**[3]** Module is available as `window.smm`

**[4]** Create new module:

```javascript
window.sm = window.sm || [];
sm.push(function myFirsModule(){
    return {
        msg: "Hello World!"
    }
});
```
_( ↑ file `myFirsModule.js`)_

**[5]** Create second module:

```javascript
window.sm = window.sm || [];
sm.push(function mySecondModule(_modules){
    if(_modules['@myFirsModule']){
	    return {
	        msg: "Hello World!"
	    }
    }
});
```
_( ↑ file `mySecondModule.js`)_

**[6]** Add for HTML file:

```html
<script type="text/javascript" src="myFirsModule.js" async></script>
<script type="text/javascript" src="mySecondModule.js" async></script>
```

## ❖ Use in Node.js ❖

```javascript
import { init as initSMM } from "simple-modules-manager/smm";
initSMM ('global', 'smm');
```

or

```javascript
var smm = require("simple-modules-manager/smm");
smm.init('global', 'smm');
```


## ❖ Development ❖

### Running the tests:

For browser:

 1. Run in terminal `npm run rollup:test && npm run browserify:test`
 2. Run server (in terminal) `npm run server`
 3. Open first link [test.browserify.html](http://127.0.0.1:8080/test.browserify.html)
 4. Open second link [test.rollup.html](http://127.0.0.1:8080/test.rollup.html)
 5. If in terminal _(console)_ not show ERROR, test is OK.

## ❖ Versioning ❖

I use the versioning system [SemVer](http://semver.org/) _(2.0.0)_

## ❖ Author❖ 

* **Adrian Gargula**

## ❖ License ❖

This project is licensed under the ISC License - see the [wiki/ISC_license](https://en.wikipedia.org/wiki/ISC_license)

## ❖ Other ❖

This project is based on [Adrosar/ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) [_(version 2.0.0)_](https://bitbucket.org/Adrosar/ts-startek-kit/src/2.0.0/)
